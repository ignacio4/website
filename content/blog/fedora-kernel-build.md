+++
title = "Build a fedora kernel: Updated"
description = "Patches you might require won't always be backported - this post will help you build a kernel with the required patches"
date = 2021-03-02
template = "page/default.html"
draft = false
+++

The primary source of information on building a custom kernel in fedora is [here](https://fedoraproject.org/wiki/Building_a_custom_kernel#Building_a_Kernel_from_the_Fedora_source_tree). This post will step you through that with an additional step to add the patches you may need.

## Get the source

First, where-ever you feel like, clone the kernel packaging source, for example:

```bash
cd ~ && mkdir projects && cd ~/projects
fedpkg clone -a kernel
```

Then cd in to the new directory and checkout the required branch matching the fedora release number you need:

```bash
cd ~/projects/kernel
git checkout origin/f33
```
you can also checkout `f34` which will be the 5.11 kernel, and what the more recent patches will apply to.

To save work with git (not a compulsory step, but will help with updating), do:
```
git checkout -b patch-f33
git branch -u origin/f33
```

## Prepare for build

### Install deps

Run:
```
sudo dnf install fedpkg fedora-packager rpmdevtools ncurses-devel pesign grubby
sudo dnf builddep kernel.spec
```

### Edit kernel.spec

In `kernel.spec` is where you will do most of the work, so open up that file in your
favourite text editor which is vim and search for this line:

```bash
# define buildid .local
```

and replace it with:

```bash
%define buildid .local
```

then a few hundred or so lines further down the file you will find lines like this one:

```bash
Patch126: 0001-Work-around-for-gcc-bug-https-gcc.gnu.org-bugzilla-s.patch
```
don't worry if that particular line doesn't exist, look for `Patch1` or `Patch`, case-sensitive.
This is where you will need to add patches, an example is adding the `asus-hid` driver
patch required for ASUS laptops with the 0x1866 device (N-Key Keyboard). So below the
line above add:

```bash
# ROG Laptops
Patch500: 0001-WMI-asus-Reduce-G14-and-G15-match-to-min-product-nam.patch
Patch501: 0001-HID-asus-Filter-keyboard-EC-for-old-ROG-keyboard.patch
Patch502: 0001-HID-asus-Add-support-for-2021-ASUS-N-Key-keyboard.patch
```

and lastly, grab the patches - they must be in the same directory as `kernel.spec`:
1. [0001-WMI-asus-Reduce-G14-and-G15-match-to-min-product-nam.patch](/patches/0001-WMI-asus-Reduce-G14-and-G15-match-to-min-product-nam.patch)
2. [0001-HID-asus-Filter-keyboard-EC-for-old-ROG-keyboard.patch](/patches/0001-HID-asus-Filter-keyboard-EC-for-old-ROG-keyboard.patch)
3. [0001-HID-asus-Add-support-for-2021-ASUS-N-Key-keyboard.patch](/patches/0001-HID-asus-Add-support-for-2021-ASUS-N-Key-keyboard.patch)

and then build using `fedpkg --release f33 local` (remember to replace f33 with the branch you checked out).

You can also build non-debug with:
```
make release
fedpkg --release f33 local
```

## Install

All the rpm packages will be located in `~/projects/kernel/x86_64/`, it's likely that
you will need to install all except the debug packages.

## Updating

If you have any work to save, `git commit` it, or `git stash`.

```
git pull
```

then `git stash pop` if you stashed work.
