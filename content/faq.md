+++
title = "Frequently Asked Questions"
template = "page/default.html"
+++

# asus-nb-ctrl

**Is `<distro>` supported by asus-nb-ctrl?**

Only Fedora 33 and OpenSUSE Tumbleweed officially. Other distros may have
packaging, or you will need to compile and install it yourself.

#### It's not working!

Check the logs with `sudo journalctl -b -u asusd` and look for errors.

#### There's an error like:

 ````
 Error: D-Bus error: The name org.asuslinux.Daemon was not provided by any .service files (org.freedesktop.DBus.Error.ServiceUnknown)
 ````
  when I run `asusctl <command>`

The daemon isn't running, check the logs with `sudo journalctl -b -u asusd` and look for errors.

#### Error is something about a keyboard?

Is the keyboard driver loaded? Use `lsmod |grep rog` to look for `hid_asus_rog`
if it's not there the DKMS module may need to be forced rebuild and installed.

This is a common issue after kernel upgrades.

#### I don't have profile or charge control

This is usually to do with the WMI driver. You will need to either install
[asus-rog-nb-wmi](https://gitlab.com/asus-linux/asus-rog-nb-wmi) form that git
repo or follow [this guide](https://asus-linux.org/wiki/keyboard-leds-anime/#installing).

If it is installed you may need to force rebuild the DKMS module, see further down.

#### Is custom fan-curve supported?

Yes, but only on Ryzen laptops. You need `acpi_call` kernel module installed via
any method applicable to your distro, and likely need add a file:
```
# Filename: /etc/modules-load.d/acpi-call.conf
acpi_call
```
which will load the module on boot.

#### How do I set a custom fan curve?

The format is shown [here](https://github.com/cronosun/atrofac/blob/master/ADVANCED.md#limits). 

#### FN+F5 doesn't do anything?

You need to map the key-combo to an action in your desktop, like this:

![](/images/fan-shortcut.png)

# Graphics and Switching

#### Why did nvidia mode give me black screen with xorg?

This happens on AMD machines typically, if your machine is AMD then you need to
add the following to run on graphical login:

```
xrandr --setprovideroutputsource modesetting NVIDIA-0
xrandr --auto
```

there are many ways to do this so you will need to search for the right way for
your login manager.

**KDE/SDM NOTES** The location of the startup script for KDE/SSM is wrong on most resources found on the internet: To get the login screen to show up on the laptop screen when in Nvidia mode, the xrandr commands must be added to /etc/sddm/Xsetup (NOT /usr/share/sddm/scripts/Xsetup which most resources mention). (thanks `motoridersd `)

#### Switching to integrated/hybrid/nvidia  doesn't seem to work

There's a few steps to troubleshoot.

1. Are your Nvidia drivers installed correctly?

In hybrid or nvidia mode they should be loaded, check with `lsmod |grep nvidia` for similar to:
```
[luke@datatron]$ lsmod |grep nvidia
nvidia_drm             65536  11
nvidia_modeset       1232896  18 nvidia_drm
nvidia_uvm           1150976  0
nvidia              34185216  906 nvidia_uvm,nvidia_modeset
drm_kms_helper        274432  2 nvidia_drm,i915
drm                   618496  14 drm_kms_helper,nvidia_drm,i915
```
if that doesn't show similar, then you need to reinstal your Nvidia drivers.

2. Is Nouveau blocklisted correctly? It's common to require the following in `/etc/default/grub`:
```
GRUB_CMDLINE_LINUX="rd.driver.blacklist=nouveau modprobe.blacklist=nouveau"
```
that line may not be exact, and *do not* delete existing entry to match it, just add those two items to the end of the line before the last "

3. What does `journalctl -b -u asusd` say after a failed switch?

If it contains lines like the following then we need to look at active sessions
```
asusd[1373]: INFO: GFX: Switching gfx mode to integrated
asusd[1373]: INFO: GFX: display-manager thread started
asusd[1373]: WARN: GFX: thread time exceeded 3 minutes, exiting
```
Do: `loginctl list-sessions`, if it shows more than one session lets have a look with:
```
loginctl show-session --property Type --property Class --property Active <num>
```
where `<num>` is the number or char+num from the `SESSION` column of the output above.
If there is more than one `active` session of type `Wayland | X11 | Mir` then somehow
you've ended up with an extra session.

Save your work, `Ctrl+Alt+F4` (or any F`<num>`) key to a TTY terminal session and run
```
asusctl graphics -m <desired mode>
sudo loginctl kill-user <username>
```
your display-manager should then restart.

4. If none of the above helps, and/or you use KDE, and/or have a G14 with GTX
graphics, you may need to this,
```
sudo sed -i 's/#KillUserProcesses=no/KillUserProcesses=yes/' /etc/systemd/logind.conf
```
then `sudo systemctl restart display-manager`.

# Keyboard

#### My keyboard is not working with the driver?

You may have a different keyboard. Please request support in one of the
related projects on gitlab, or in the discord server.

#### Mic-Mute doesn't work?

The issue is that the current keycode being emitted by the driver is only
recognised by Wayland. X11 and desktops using X11 require `F20` to be emitted.

Create a file named `/etc/udev/hwdb.d/90-nkey.hwdb` with:

```
evdev:input:b0003v0B05p1866*
  KEYBOARD_KEY_ff31007c=f20 # x11 mic-mute
```

then update hwdb with:

```
sudo systemd-hwdb update
sudo udevadm trigger
```

#### Can I customise the `Fn` key?

No, the key is on a physically different circuit and used to *physically*
signal the keyboard EC to switch key circuits. There are three different
circuits for the `0x8166` keyboard.

#### I have a G14/G15 or other laptop where the Arrow keys are unmarked by emit keycodes, can I use these?

Yes

#### Can I remap the arrow-keys?

A1: Yes, create a file named `/etc/udev/hwdb.d/90-nkey.hwdb` with:

```
evdev:input:b0003v0B05p1866*
  KEYBOARD_KEY_c00b6=kbdillumdown # Fn+F2 (music prev)
  KEYBOARD_KEY_c00b5=kbdillumup   # Fn+F4 (music skip)
  KEYBOARD_KEY_ff3100c5=pagedown  # Fn+Down
  KEYBOARD_KEY_ff3100c4=pageup    # Fn+Up
  KEYBOARD_KEY_ff3100b2=home      # Fn+Left
  KEYBOARD_KEY_ff3100b3=end       # Fn+Right
```

then update hwdb with:

```
sudo systemd-hwdb update
sudo udevadm trigger
```

You can see a list of keycodes [here](https://github.com/torvalds/linux/blob/b76f733c3ff83089cf1e3f9ae233533649f999b3/include/uapi/linux/input-event-codes.h).

#### How do I rebuild the DKMS modules

A:

```
sudo dkms remove hid-asus-rog/0.6.0 --all
sudo dkms build hid-asus-rog/0.6.0
sudo dkms install hid-asus-rog/0.6.0
```

You will need to replace the `0.6.0` with the module version. To find the version
try `ls /usr/src/` and the number showing such as `hid-asus-rog-0.6.0-1.1` is the
version. So for the above commands here you would replace `0.6.0` with `0.6.0-1.1`.

The same applies to `hid-asus-rog`.

# Desktop Notifications

#### How do I get feedback for Fn+Key presses?

asus-nb-ctrl comes with a notification daemon and service you can enable:

```
systemctl --user enable asus-notify.service
systemctl --user start asus-notify.service
```

this works fine for almost all desktops and provides basic notifications through
dbus.

There is also:
- [Gnome extension](https://gitlab.com/asus-linux/asus-nb-gex)
- [asusctltray, tray icon tool](https://github.com/Baldomo/asusctltray)

# AniMe Matrix Display

 - [ZephyrusBling](https://github.com/Meumeu/ZephyrusBling)