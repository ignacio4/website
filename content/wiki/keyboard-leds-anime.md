+++
title = "General Setup"
description = "All distros. Setting up the N-Key keybord, RGB keyboard backlight, and AniMe matrix"
sort_by = "none"
template = "page/wiki.html"
author = "Luke Jones"
+++

In the begining there was light. But it was not controlled. Then rog-core was birthed and all was bright and colourful. Eventually rog-core grew arms and legs and sort of power-profiles and other system control features.

There were issues though, `rog-core` couldn't really recover from some problems such as the device sleeping or being late to initialise, and to fix these problems would take too much effort and time for little gain.

Some of `rog-core` was stripped out and moved to the `asus-hid` kernel module, which is in the process of being upstreamed (specifically the keyboard init sequence and all Fn key functions). This has allowed for a far better and more robust keyboard driver, and for `rog-core` to be depreciated for the `asus-nb-ctrl` fork and rewrite of it.

`asus-nb-ctrl` currently controls:

- CPU power profiles according to fan mode level
- Battery charge limit setting
- RGB keyboard modes
- AniMe matrix display (CLI tools coming soon)

More features will appear as time goes on. Do note that `rog-core` is depreciated and will not be supported any longer.

# [Installing](#installing)

Currently there are either two or three parts to install depending on your laptop.

1. `hid-asus-rog` dkms module for the keyboard. **This is not required for kernel version 5.11+**.
2. `asus-rog-nb-wmi` dkms module for G14-G15 support. **This is not required for kernel version 5.9+**.
3. `asus-nb-ctrl`, not a hard requirement but does some nice things

A quick run of `lsusb | grep 1866` in a terminal will tell you if you have the N-Key type keyboard on USB, if you do then `asus-hig-rog` dkms module will work for you.

**NOTE:** You will have an easier time if you install your distro Nvidia drivers
first. The same applies to manually installed Nvidia drivers.

## [Supported distros](#supported-distros)

As of now, the only officially supported distributions are Fedora 33 and OpenSUSE Tumbleweed. Arch is *not* supported, and I (the maintainer of asus-nb-ctrl) will no longer be helping troubleshoot it.

The DKMS modules are packaged for:
- Fedora 32, 33
- OpenSUSE Tumbleweed
- Ubuntu 20.04, 20.10

`asus-nb-ctrl` is packaged officially only for:
- Fedora 33
- OpenSUSE Tumbleweed

## [First steps](#first-steps)

You must first:
- update your distro
- install Nvidia drivers
- remove any distro provided methods of graphics switching (like prime)
- reboot!

## [fedora](#fedora)

The easiest way to install is to add the provided repo. Create the file `/etc/yum.repos.d/asus.repo` containing:

```
[asus]
name=asus
failovermethod=priority
baseurl=https://download.opensuse.org/repositories/home:/luke_nukem:/asus/Fedora_33/
enabled=1
gpgcheck=0
```

You can then install all required packages with:

```
$ dnf update --repo asus --refresh
$ dnf install asus-nb-ctrl dkms-hid-asus-rog dkms-asus-rog-nb-wmi
```
then `Reboot!`

## [openSUSE](#opensuse)

Run:

```
$ zypper ar --priority 50 --refresh https://download.opensuse.org/repositories/home:/luke_nukem:/asus/openSUSE_Tumbleweed/ asus-linux
$ zypper ref
$ zypper in asus-nb-ctrl dkms-hid-asus-rog dkms-asus-rog-nb-wmi
```
then `Reboot!`

## [ubuntu](#ubuntu) - Modules only

Run:

```
$ echo "deb https://download.opensuse.org/repositories/home:/luke_nukem:/asus/xUbuntu_20.04/ /" > /etc/apt/sources.list.d/asus.list
$ wget -q -O - https://download.opensuse.org/repositories/home:/luke_nukem:/asus/xUbuntu_20.04/Release.key | apt-key add -
$ apt-get update
$ apt-get install dkms-hid-asus-rog dkms-asus-rog-nb-wmi
```

Replace `20.04` with `20.10` as required. `Reboot!`

`asus-nb-ctrl` on ubuntu requires manual building with rust installed via [rustup.rs](https://rustup.rs/), and follow directions [here](https://gitlab.com/asus-linux/asus-nb-ctrl#building).

## [Other distros](#other-distros)

Are you willing to create and maintain packaging? Open an issue on the [website repo](https://gitlab.com/asus-linux/website/-/issues).

# [Manual install](#manual-install)

For the DKMS modules you will need to install your distros dev package for the kernel, make tools, dkms, and C compiler.

## [hid-asus-rog](#hid-asus-rog)

1. `git clone https://gitlab.com/asus-linux/hid-asus-rog && cd hid-asus-rog`
2. `dkms add .`
3. `dkms build hid-asus-rog/1.0.0`
4. `dkms install hid-asus-rog/1.0.0`

and then reboot. On kernel load you should see the keyboard backlight flick as the keyboard is initialised, you can also test the keys such as screen-brightness, keyboard backlight up/down etc. At this point if you only need keyboard functions then you're done.

## [asus-rog-nb-wmi](#asus-rog-nb-wmi)

1. `git clone https://gitlab.com/asus-linux/asus-rog-nb-wmi && asus-rog-nb-wmi`
2. `dkms add .`
3. `dkms build asus-rog-nb-wmi/1.0.0`
4. `dkms install asus-rog-nb-wmi/1.0.0`

## [asus-nb-ctrl](#asus-nb-ctrl)

Please see the [repository](https://gitlab.com/asus-linux/asus-nb-ctrl) for guidance.

# [Updating](#updating)

If you added the distro repos then you will get updates the usual way. If you manually installed then you will need to use `git pull` and follow the steps for build and install.

If you update your kernel while you have the dkms modules installed then you should not need to do anything, dkms will rebuild the modules for your kernel automatically.

# [Finished](#finished)

Reboot and congratulations, everything is running now. Using `asusctl --help` will provide some instructions on how/what to set up via CLI. You can also edit the following files:

- `/etc/asusd/asusd-ledmodes.toml`, only useful for RGB keyboards, edit this to add or change supported modes and laptops
- `/etc/asusd/asusd.conf`, for all laptops


*Note:* at some point the work done in the DKMS modules will make it in to the mainstream kernel, making the DKMS modules obsolete for that kernel version and above - the patches have been submitted and it is a matter of time. When that finally happens, we will add a note here.

*Note2:* systemd can sometimes be an issue for asusd in that asusd is possible to be loaded before the required modules are loaded. This issue will manifest as errors about missing functionality in the journal log. To counteract this you can add the following file:

```
# /etc/modules-load.d/asus.conf

asus_wmi
asus_nb_wmi
asus_rog_nb_wmi
hid_asus_rog
```
