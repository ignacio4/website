+++
title = "Misc Information"
description = "Advisory items that don't fit any particular place"
sort_by = "none"
template = "page/wiki.html"
author = "Luke Jones"
+++

## 2021 Ryzen G14/G15

```
iGPU Off: Using Microsoft Hybrid Graphics mode, discrete Graphics priority first.

iGPU On: The system will disable the discrete GPU，and only use internal GPU.

Auto: When the external display is not used for output purposes "iGPU mode" will be turned on automatically in battery mode, and will automatically turm off when your computer is connected to a power source.
```

iGPU On: carries across boots and disables the dGPU. As such this will impact
hybrid and nvidia graphics mode sin Linux. The method of turning it off
isn't known yet and may help to enable hybrid mode powersaving.

## 2020 G14/G15 experiencing random sound issues

Typically after a Windows boot. The windows driver sets up the Realtek codec chip
a particular way which isn't replicated exactly in the Linux kernel driver for it
yet. You will need to do a *cold* boot by powering off and unplugging for a minute
or so - if this doesn't solve it then a hard power-off in Linux by holding the
power-button until powered off may help.